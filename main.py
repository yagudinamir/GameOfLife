import Script
import argparse
import sys

# in_file = "input.txt"
# out_file = "output.txt"


def run(infile, outfile):
    """
    Running the program, getting input from infile &&
    output from outfile
    :param infile:
    :param outfile:
    """
    if infile != sys.stdin:
        infile = open(infile, 'r')
    if outfile != sys.stdout:
        outfile = open(outfile, 'w')
    _input = Script.input_from_file(infile)
    _output = Script.process(_input)
    Script.output_to_file(outfile, _output)


if __name__ == '__main__':
    """
    Main function of the projects
    parsing args && running the program
    """
    parser = argparse.ArgumentParser()

    parser.add_argument("-r", "--read", nargs='?', default=sys.stdin, help="""
                        reads input from the given file name (stdin by default)""")

    parser.add_argument("-w", "--write", nargs='?', default=sys.stdout, help="""
                        writes output to the given file name (stdout by default)""")

    # parser.add_argument("r", type=str, help="in_file")
    # parser.add_argument("w", type=str, help="out_file")
    args = parser.parse_args()

    run(args.read, args.write)
