import abc
from enum import Enum


class Cell(metaclass=abc.ABCMeta):
    @abc.abstractmethod
    def update_cell(self, neighbours):
        """
        Overviewing neighbours, for the next iteration
        :return:
        object of the class
        """

    def get_cell_type(cls):
        """
        Getiing a character for the class
        :return:
        exact enum
        """
        return cls.ch


class Creatures(Enum):
    FISH = "f"
    CRAYFISH = "s"
    ROCK = "r"
    EMPTYCELL = "n"

    def __str__(self):
        """
        A string representation of the enum
        :return: "f", "s", "r" or "n"
        """
        return self.value


class Fish(Cell):
    def __init__(self):
        """
        Initing Fish
        """
        self.ch = Creatures.FISH

    def update_cell(self, neighbours):
        count = neighbours.count(Creatures.FISH)

        if count >= 4 or count < 2:
            return EmptyCell()
        else:
            return Fish()


class CrayFish(Cell):
    def __init__(self):
        """
        Initing CrayFish
        """
        self.ch = Creatures.CRAYFISH

    def update_cell(self, neighbours):
        count = neighbours.count(Creatures.CRAYFISH)

        if count >= 4 or count < 2:
            return EmptyCell()
        else:
            return CrayFish()


class Rock(Cell):
    def __init__(self):
        """
        Initing Rock
        """
        self.ch = Creatures.ROCK

    def update_cell(self, neighbours):
        return Rock()


class EmptyCell(Cell):
    def __init__(self):
        """
        Initing EmptyCell
        """
        self.ch = Creatures.EMPTYCELL

    def update_cell(self, neighbours):
        f_count = neighbours.count(Creatures.FISH)
        s_count = neighbours.count(Creatures.CRAYFISH)

        if f_count is 3:
            return Fish()
        elif s_count is 3:
            return CrayFish()
        else:
            return EmptyCell()


class Ocean:
    def __init__(self, n_, m_):
        """
        Initing Ocean with height n_ && widht m_, with EmptyCell objects
        :param n_:
        :param m_:
        """
        self.n = n_
        self.m = m_
        self.massive = [[EmptyCell()] * self.m for _ in range(self.n)]

    def mas_init(self, _massive):
        """
        Initializing the massive of the ocean
        :param _massive:
        """
        for i in range(self.n):
            for j in range(self.m):
                if _massive[i][j] == "f":
                    _massive[i][j] = Fish()
                elif _massive[i][j] == "s":
                    _massive[i][j] = CrayFish()
                elif _massive[i][j] == "r":
                    _massive[i][j] = Rock()
                else:
                    _massive[i][j] = EmptyCell()
        self.massive = _massive

    def is_valid(self, x, y):
        """
        Cheking whether the point is on the massive
        :param x:
        :param y:
        :return: bool
        """
        return 0 <= x < self.n and 0 <= y < self.m

    def get_neighbours(self, x, y):
        """
        getting neighbours of the square
        :param x:
        :param y:
        :return: list of the enums
        """
        neighbours = []
        for i in range(x - 1, x + 2):
            for j in range(y - 1, y + 2):
                if (i != x or j != y) and self.is_valid(i, j):
                    neighbours.append(self.massive[i][j].get_cell_type())
        return neighbours

    def _iterate(self):
        """
        Iterating one time
        :return: list
        """
        _massive = [[0] * self.m for _ in range(self.n)]
        for i in range(self.n):
            for j in range(self.m):
                _massive[i][j] = self.massive[i][j].update_cell(self.get_neighbours(i, j))
        return _massive

    def run(self, gen):
        """
        Iterating gen times
        :param gen:
        :return:
        """
        for _ in range(gen):
            self.massive = self._iterate()

    def to_list(self):
        """
        Getting the representation of the ocean
        :return: list of the lines
        """
        output = []
        for i in range(self.n):
            s = ''
            for j in range(self.m):
                s = s + str(self.massive[i][j].get_cell_type())
            output.append(s)
        return output
