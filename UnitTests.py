import pytest
import Lib
import main


def test_fish():
    fish = Lib.Fish()
    assert fish.get_cell_type() == Lib.Creatures.FISH


def test_crayfish():
    crayfish = Lib.CrayFish()
    assert crayfish.get_cell_type() == Lib.Creatures.CRAYFISH


def test_rock():
    rock = Lib.Rock()
    assert rock.get_cell_type() == Lib.Creatures.ROCK


def test_creatures():
    creature = Lib.Creatures.FISH
    assert str(creature) == "f"


def test_updating_cell():
    cell = Lib.EmptyCell()
    neighbours = [Lib.Creatures.FISH, Lib.Creatures.FISH, Lib.Creatures.FISH,
                  Lib.Creatures.ROCK, Lib.Creatures.ROCK, Lib.Creatures.ROCK]
    assert type(cell.update_cell(neighbours)) == Lib.Fish


def test_initing_ocean():
    ocean = Lib.Ocean(3, 4)
    ocean.mas_init([["f", "f", "f", "f"], ["f", "f", "f", "f"], ["f", "f", "f", "f"]])
    for i in range(3):
        for j in range(4):
            assert type(ocean.massive[i][j]) == Lib.Fish


def test_to_list():
    ocean = Lib.Ocean(3, 4)
    ocean.mas_init([["f", "f", "n", "n"], ["f", "s", "s", "r"], ["s", "s", "s", "s"]])
    assert ocean.to_list() == ["ffnn", "fssr", "ssss"]


def test_full():
    with open('input.in', 'w') as f:
        f.write("3 5 1\n")
        f.write("fnsrn\n")
        f.write("sssff\n")
        f.write("nrrsf")
    main.run('input.in', 'output.out')
    with open('output.out') as g:
        ans = g.readlines()
    assert ans == ["nnsrn\n", "nssff\n", "nrrnf\n"]
