import Lib


def input_from_file(_file):
    """
    Getting an ocean from the _file
    :param _file:
    :return: list
    """
    s = []
    for line in _file.readlines():
        s.append(line.strip())
    return s


def output_to_file(_file, s):
    """
    Writing the ocean massive s to the _file
    :param _file:
    :param s:
    """
    for i in range(len(s)):
            _file.write(str(s[i]))
            _file.write('\n')
    _file.close()


def process(_input):
    """
    The main process of the program;
    _input is a list of strings
    :param _input:
    :return: list of strings
    """

    n, m, g = map(int, (_input[0].strip()).split())
    mas = []
    for i in range(1, n + 1):
        s = []
        for j in range(m):
            s.append(_input[i][j])
        mas.append(s)

    ocean = Lib.Ocean(n, m)
    ocean.mas_init(mas)

    ocean.run(g)

    return ocean.to_list()
